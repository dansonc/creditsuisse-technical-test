package creditsuisse;

import java.util.HashMap;
import java.util.Map;

class Epos {

    private static final int SESSION_ID_BASE = -1;
    private int sessionId = SESSION_ID_BASE;

    private Session currentSession;
    private final Map<Integer, Session> sessions = new HashMap<>();
    private final Map<Item, PriceRetriever> priceRetrievers;


    Epos(Map<Item, PriceRetriever> priceRetrievers) {

        this.priceRetrievers = priceRetrievers;
    }

    int sessionStart() {
        currentSession = new Session();
        sessions.put(++sessionId, currentSession);

        return sessionId;
    }

    void scan(Item item) {
        currentSession.add(item, priceRetrievers.get(item));
    }

    double getSessionTotal(int sessionId) {
        return sessions.get(sessionId).getTotal();
    }
}
