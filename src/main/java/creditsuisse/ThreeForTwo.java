package creditsuisse;

import java.time.LocalDate;

public class ThreeForTwo extends Offer{

    ThreeForTwo(LocalDate localDate) {
        super(localDate);
    }

    @Override
    public boolean applies(int scannedCount) {
        return scannedCount % 3 == 0 && notExpired();
    }

}
