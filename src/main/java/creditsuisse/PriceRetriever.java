package creditsuisse;

interface PriceRetriever {

    double getPrice(int scannedCount);
}
