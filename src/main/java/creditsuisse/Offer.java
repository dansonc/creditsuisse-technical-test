package creditsuisse;

import java.time.LocalDate;

abstract public class Offer implements Promotion{

    private final LocalDate expiry;

    Offer(LocalDate expiry) {
        this.expiry = expiry;
    }

    abstract public boolean applies(int scannedCount);

    @Override
    public double price() {
        return 0; // Override where necessary to implement other offer prices
    }

    boolean notExpired() {
        return expiry.isAfter(LocalDate.now());
    }
}
