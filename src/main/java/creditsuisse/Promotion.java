package creditsuisse;

public interface Promotion {

    Promotion NO_PROMOTION = new Promotion() {
        @Override
        public boolean applies(int scannedCount) {
            return false;
        }

        @Override
        public double price() {
            return 0;
        }
    };

    boolean applies(int scannedCount);

    double price();
}
