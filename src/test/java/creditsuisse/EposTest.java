package creditsuisse;

import org.junit.Before;
import org.junit.Test;


import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static creditsuisse.Item.*;
import static creditsuisse.Promotion.NO_PROMOTION;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

// TODO - consider how to Void an item

public class EposTest {

    private static final double PRECISION = 0000000000.1;

    private int currentSessionId;

    private Map<Item, PriceRetriever> priceRetrievers = mock(HashMap.class);

    private final Epos epos = new Epos(priceRetrievers);

    private final PriceRetriever limePriceRetriever = new Sku(new ThreeForTwo(LocalDate.ofYearDay(2017, 365)), 0.15);
    private final PriceRetriever melonPriceRetriever = new Sku(new BuyOneGetOneFree(LocalDate.ofYearDay(2017, 365)), 0.50);
    private final PriceRetriever bananaPriceRetriever = new Sku(NO_PROMOTION, 0.20);
    private final PriceRetriever applePriceRetriever = new Sku(NO_PROMOTION, 0.35);

    private final PriceRetriever limePriceRetrieverWithExpiredOffer = new Sku(new ThreeForTwo(LocalDate.ofYearDay(2017, 1)), 0.15);
    private final PriceRetriever melonPriceRetrieverWithExpiredOffer = new Sku(new BuyOneGetOneFree(LocalDate.ofYearDay(2017, 1)), 0.50);

    @Before
    public void setup() {

        when(priceRetrievers.get(LIME)).thenReturn(limePriceRetriever);
        when(priceRetrievers.get(MELON)).thenReturn(melonPriceRetriever);
        when(priceRetrievers.get(BANANA)).thenReturn(bananaPriceRetriever);
        when(priceRetrievers.get(APPLE)).thenReturn(applePriceRetriever);
    }

    @Test
    public void scan1Lime(){
        currentSessionId = epos.sessionStart();

        scanItems(LIME);

        assertThat(epos.getSessionTotal(currentSessionId), is(0.15));
    }

    @Test
    public void scan2Limes(){
        currentSessionId = epos.sessionStart();

        scanItems(LIME, LIME);

        assertThat(epos.getSessionTotal(currentSessionId), is(0.30));
    }

    @Test
    public void scan3LimesWithinOfferPeriod(){
        currentSessionId = epos.sessionStart();

        scanItems(LIME, LIME, LIME);

        assertThat(epos.getSessionTotal(currentSessionId), is(0.30));
    }

    @Test
    public void scan3LimesWithinOfferPeriodAnd1Melon(){
        currentSessionId = epos.sessionStart();

        scanItems(LIME, LIME, LIME, MELON);

        assertThat(epos.getSessionTotal(currentSessionId), is(0.80));
    }

    @Test
    public void scan3LimesWithinOfferPeriodAnd2MelonsWithinOfferPeriod() {

        currentSessionId = epos.sessionStart();
        scanItems(LIME, LIME, LIME, MELON, MELON);

        assertThat(epos.getSessionTotal(currentSessionId), is(0.80));
    }

    @Test
    public void scan3LimesWithinOfferPeriodAnd2MelonsAfterOfferHasExpired() {
        when(priceRetrievers.get(MELON)).thenReturn(melonPriceRetrieverWithExpiredOffer);

        currentSessionId = epos.sessionStart();
        scanItems(LIME, LIME, LIME, MELON, MELON);

        assertThat(epos.getSessionTotal(currentSessionId), is(1.30));
    }

    @Test
    public void scan1BananaAnd1Apple(){
        currentSessionId = epos.sessionStart();

        scanItems(BANANA, APPLE);

        assertThat(epos.getSessionTotal(currentSessionId), is(0.55));
    }

    @Test public void scan3LimesAfterOfferHasExpired(){

        when(priceRetrievers.get(LIME)).thenReturn(limePriceRetrieverWithExpiredOffer);
        currentSessionId = epos.sessionStart();

        scanItems(LIME, LIME, LIME);

        assertThat(epos.getSessionTotal(currentSessionId), closeTo(0.45, PRECISION));
    }

    private void scanItems(Item ... items) {
        for(Item item : items){
            epos.scan(item);
        }
    }
}
